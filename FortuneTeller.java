package FortuneTellerPackage;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by aaron on 11/2/15.
 */
public class FortuneTeller {

    String [] howResponses = {"Use me(computer) to research", "You determine the how", "Trust your instincts", "Be creative, figure it out", "I honestly do not know"};
    String [] whatResponses = {"I do not know, you tell me", "Whatever your heart desire", "Let me get back to you on that", "Why you asking me?", "Nothing"};
    String [] whenResponses = {"2-5 years from now", "Whenever you make it happen", "Hmmmm let me think about it....Tomorrow", "In 3-5 months", "Right Now", "Time will tell"};
    String [] whereResponses = {"On the side of the road", "Cancun", "Wherever you want it", "Your dream location", "At a Church"};
    String [] amResponses = {"Yes", "No", "I do not know", "Maybe", "You are the smart person, I am just a computer"};
    String [] whoResponses = {"You best friend", "The person you are with", "How do I know, I am not living your life...", "Next door neighbor", "Someone you would never guess"};

    private String response;

    protected String getHowResponses(){
        response = howResponses[ThreadLocalRandom.current().nextInt(0, howResponses.length)];
        return response;
    }

    protected String getWhatResponses(){
        response = whatResponses[ThreadLocalRandom.current().nextInt(0, whatResponses.length)];
        return response;
    }

    protected String getWhenResponses(){
        response = whenResponses[ThreadLocalRandom.current().nextInt(0, whenResponses.length)];
        return response;
    }

    protected String getWhoResponses(){
        response = whoResponses[ThreadLocalRandom.current().nextInt(0, whoResponses.length)];
        return response;
    }

    protected String getWhereResponses(){
        response = whereResponses[ThreadLocalRandom.current().nextInt(0, whereResponses.length)];
        return response;
    }

    protected String getAmResponses(){
        response = amResponses[ThreadLocalRandom.current().nextInt(0, amResponses.length)];
        return response;
    }

}
