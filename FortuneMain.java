package FortuneTellerPackage;

import java.util.Scanner;

/**
 * Created by aaron on 11/2/15.
 */
public class FortuneMain {
    public static void main(String[] args) {
        FortuneTeller fortune = new FortuneTeller();
        String userInput;
        String lowerCase;

        do {
            System.out.println("Ask me your fortune, and I will tell you" + "\n Press q to quit");
            Scanner input = new Scanner(System.in);
            userInput = input.next();

            lowerCase = userInput.toLowerCase();

                if(lowerCase.contains("how")) {
                    System.out.println(fortune.getHowResponses());
                }
                else if(lowerCase.contains("who")) {
                    System.out.println(fortune.getWhoResponses());
                }
                else if(lowerCase.contains("when")) {
                    System.out.println(fortune.getWhenResponses());
                }
                else if(lowerCase.contains("what")) {
                    System.out.println(fortune.getWhatResponses());
                }
                else if(lowerCase.contains("where")) {
                    System.out.println(fortune.getWhereResponses());
                } else if(lowerCase.contains("am")) {
                    System.out.println(fortune.getAmResponses());
                } else{
                    if(lowerCase.contains("q")){
                        System.out.println("Good Bye!");
                    } else {
                        System.out.println("Ask me a real question");
                    }
                }
        } while (!lowerCase.equals("q"));

    }
}

